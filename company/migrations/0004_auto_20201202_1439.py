# Generated by Django 3.1.4 on 2020-12-02 03:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('company', '0003_auto_20201202_1418'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='username',
            field=models.CharField(blank=True, max_length=100, unique=True),
        ),
    ]

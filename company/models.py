from django.db import models

# Create your models here


class Company(models.Model):
    name  = models.CharField(max_length=200,unique=True)
    signup_date = models.DateTimeField('date signed')
    username   = models.CharField(max_length=100,blank=True,unique=True)
    bsb  = models.CharField(max_length=20)
    account  = models.CharField(max_length=30)
    email  = models.EmailField(max_length=70,blank=True,unique=True)

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'companies'


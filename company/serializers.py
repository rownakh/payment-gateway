from rest_framework import serializers
from django.contrib.auth.models import User

class CreateUserSerializer(serializers.ModelSerializer):
   def create(self,validate_data):
      user  = User.objects.create_user(**validate_data)
      #user  = User.objects.create_user()
      return user
   class Meta:
      model = User
      fields = ('id','password','username');
      extra_kwargs = {
            'password': {'write_only': True}
      }  

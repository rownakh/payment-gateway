from django.shortcuts import render
from django.http import HttpResponse
from company.models import Company
from datetime import datetime
from django.db.utils import IntegrityError

from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from django.contrib.auth.models import User
from django.http import JsonResponse
from rest_framework.response import Response
from .serializers import CreateUserSerializer
import requests

from django.views.decorators.csrf import csrf_exempt

CLIENT_ID = "SrozZwBqGQUIwJmdxESmsBHK7IzvHrdNMcCezgS0"
CLIENT_SECRET = "JuCL7sXJscVuJ4hMIJAqzVqrU5z8zC9l0VSYCpZeduMsoUZP8cRkZ0hHZKdwdHvzWwSikB98VEwRslwgxlw4eOTACsKKubn5O3l0dr73CA1IbgdZeXs7iRZXVQb3sP1b"
def index(request):
    # message = request.GET.get('msg')
    message = ""
    if request.POST:
        try:
            data = request.POST

            a_company = Company(name=data.get('name'), bsb=data.get('bsb'),username=data.get('username'),
                                account=data.get('account'), email=data.get('email'), signup_date=datetime.now())
            a_company.save()
            message = "Company Info Saved Successfully"


            # Create user object for authentication purpose

            user_data = {
                       'username' : data.get('username'),
                       'password': data.get('password')
                   }

            print(user_data)
            print(type(user_data))
            serializer = CreateUserSerializer(data=user_data)
            serializer.is_valid()
            print(serializer.errors)
            if serializer.is_valid():
                print("In Valid")
                # If it is valid, save the data (creates a user).
                serializer.save()
                user = User.objects.latest('pk')
                user.save()
        except IntegrityError as ie:
            message = "Duplicate key value violates unique constraint"
        except Exception as e:
            message = "Unknown Exception . User cannot be saved"


    if (message):
        return render(request, "index.html", {'message': message})
    else:
        return render(request, "index.html")


@api_view(['POST'])
@permission_classes([AllowAny])
def token(request):
    '''
    Gets tokens with username and password. Input should be in the format:
    {"username": "username", "password": "1234abcd"}
    'http://localhost/o/token/',

    '''
    try:
        r = requests.post(
            request.build_absolute_uri('/o/token/'),
            data={
                'grant_type': 'password',
                'username': request.data['username'],
                'password': request.data['password'],
                'client_id': request.data['client_id'],
                'client_secret': request.data['client_secret'],
            },
        )
    except requests.exceptions.HTTPError as err:
        raise SystemExit(err)

    return Response(r.json())
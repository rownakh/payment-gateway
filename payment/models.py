from django.db import models
from enum import Enum
from company.models import Company

class TransactionStatus(Enum):
    CREATED = "CREATED"
    SUCCESSFUL = "SUCCESSFUL"
    DISPUTED = "DISPUTED"
    FAILED = "FAILED"


class Payment(models.Model):
    invoice_no  = models.CharField(max_length=200)
    amount     = models.DecimalField(max_digits=10, decimal_places=2)
    status   = models.CharField(max_length=50)
    company = models.ForeignKey(Company, on_delete=models.CASCADE)

    def __str__(self):
        return "Invoice " + self.invoice_no


    class Meta:
        db_table = 'payments'
        unique_together = (('invoice_no', 'company'),)

# Create your models here.

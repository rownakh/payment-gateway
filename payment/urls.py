from django.urls import path

from . import views

urlpatterns = [
    path('', views.create_payment),
    path('<str:invoice_no>', views.maintain_payment),


]

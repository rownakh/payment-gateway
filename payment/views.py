from django.shortcuts import render
from django.utils.decorators import method_decorator
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny

from .models import Payment
from rest_framework.response import Response
from django.http import JsonResponse
from django.db.utils import IntegrityError
from django.views.decorators.csrf import csrf_exempt
from company.models import Company
from pprint import pprint
@api_view(['POST','DELETE','PUT'])
@method_decorator(csrf_exempt)

def create_payment(request):
    # message = request.GET.get('msg')
    message = ""

    if request.method == 'POST':
        try:
            data = request.POST
            invoice_no = data.get('invoice_no')
            amount = data.get('amount')
            username = data.get('username')
            company = Company.objects.get(username=username)
            a_payment = Payment(invoice_no=invoice_no, amount=amount, status='CREATED',company=company)
            a_payment.save()
            message = "Payment {} Created Successfully".format(invoice_no)

            # Create user object for authentication purpose

        except IntegrityError as ie:
            message = "Duplicate key value violates unique constraint"
        except Exception as e:
            message = "Unknown Exception . User cannot be saved"

    if message:
        res = {'msg': message}
        return Response(res)

@api_view(['DELETE','PUT'])
@method_decorator(csrf_exempt)

def maintain_payment(request, invoice_no):
    try:
        invoice_no = invoice_no
        current_user = request.user
        loggedin_username = current_user.username
        payment_objets = Payment.objects.filter(invoice_no=invoice_no)
        #Only delete if invoice belongs to loggedin user
        message = "Payment {}  Not found for the company {} ".format(invoice_no,loggedin_username)
        for a_payment in payment_objets:
            a_user_name =   a_payment.company.username

            if loggedin_username == a_user_name:
                if request.method == 'DELETE':
                    a_payment.delete()
                    message = "Payment {} deleted Successfully for the company ".format(invoice_no,loggedin_username)
                    #PUT to process payment. By default SUCCESSFUL. In Real life the status will come from Bank API
                elif request.method == 'PUT':
                    a_payment.status = 'SUCCESSFUL'
                    a_payment.save()
                    message = "Payment   successfully processed  for the invoice {}   of the company {} ".format(invoice_no, loggedin_username)
                break

        # Create user object for authentication purpose

    except IntegrityError as ie:
        message = "Duplicate key value violates unique constraint"
    except Exception as e:
        message = "Unknown Exception . User cannot be saved"

    if message:
        res = {'msg': message}
        return Response(res)

